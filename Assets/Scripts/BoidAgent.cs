using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Mathematics;
using UnityEngine.Jobs;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;

public struct GPUBoid 
{
    public Vector3 pos;
    public Vector3 rot;

    public Vector3 cohesion;
    public Vector3 alignment;
    public Vector3 avoidance;
    public Vector3 center;

    public int neighbors;
}

[RequireComponent(typeof(Collider))]
public class BoidAgent : MonoBehaviour
{
    // MANAGER
    private BoidManager agentManager;
    public BoidManager AgentManager { get { return agentManager; } set { agentManager = value; } }


    // TRANSFORM COMPONENT
    private Collider agentCollider;
    public Collider AgentCollider { get { return agentCollider; } }
    public Vector3 AgentPosition { get { return transform.position; } }
    public Vector3 AgentForward { get { return transform.forward; } }


    // TO UPDATE
    private Vector3 agentCohesion;
    public Vector3 AgentCohesion { get { return agentCohesion; } set { agentCohesion = value; } }
    
    private Vector3 agentAlignment;
    public Vector3 AgentAlignment { get { return agentAlignment; } set { agentAlignment = value; } }
    
    private Vector3 agentAvoidance;
    public Vector3 AgentAvoidance { get { return agentAvoidance; } set { agentAvoidance = value; } }

    private Vector3 agentCenter;
    public Vector3 AgentCenter { get { return agentCenter; } set { agentCenter = value; } }

    private Vector3 agentVelocity;
    public Vector3 AgentVelocity { get { return agentVelocity; } }
    
    private List<Transform> agentNeighborsTransform;
    public List<Transform> AgentNeighborsTransform { get { return agentNeighborsTransform; } set { agentNeighborsTransform = value; } }
    
    private int agentNeighbors;
    public int AgentNeighbors { get { return agentNeighbors; } set { agentNeighbors = value; } }


    private void Start()
    {
        agentCollider = GetComponent<Collider>();
    }

    public void UpdateBoidSettings()
    {
        if (AgentNeighbors > 0)
        {
            agentVelocity += NormalizedMovement(agentCohesion, agentManager.cohesionForce);
            agentVelocity += NormalizedMovement(agentAlignment, agentManager.alignmentForce);
            agentVelocity += NormalizedMovement(agentAvoidance, agentManager.avoidanceForce);
        }

        agentVelocity += NormalizedMovement(agentCenter, agentManager.centerForce);
    }

    public Vector3 NormalizedMovement(Vector3 behavior, float weight)
    {
        Vector3 partialMove = behavior * weight;

        if (partialMove != Vector3.zero)
        {
            if (partialMove.sqrMagnitude > weight * weight)
            {
                partialMove.Normalize();
                partialMove *= weight;
            }
        }

        return partialMove;
    }

    public void AgentNextMovement(Vector3 velocity)
    {
        velocity *= AgentManager.movingSpeed;
                    
        if (velocity.sqrMagnitude > AgentManager.SqrMaxSpeed)
            velocity = velocity.normalized * AgentManager.maxSpeed;

        this.transform.rotation = Quaternion.FromToRotation(Vector3.forward, velocity);

        this.transform.position += velocity * Time.deltaTime;
    }
}

[BurstCompile]
public struct AgentNextMoveJob : IJobParallelForTransform
{
    public NativeArray<Vector3> Movement;
    public float SqrMaxSpeed;
    public float MaxSpeed;
    public float MovingSpeed;
    public float DeltaTime;

    public void Execute(int index, TransformAccess transform)
    {
        Vector3 pos = Movement[index];
        pos *= MovingSpeed;
                        
        if (pos.sqrMagnitude > SqrMaxSpeed)
            pos = pos.normalized * MaxSpeed;

        transform.rotation = Quaternion.FromToRotation(Vector3.forward, pos);
        transform.position += pos * DeltaTime;
    }
}