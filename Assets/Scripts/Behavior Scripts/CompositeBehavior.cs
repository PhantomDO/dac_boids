using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;

[CreateAssetMenu(menuName = "Boid/Behavior/Composite")]
public class CompositeBehavior : BoidBehavior
{
    public BoidBehavior[] behaviors;
    public float[] weights;


    public override Vector3 CalculateMove(BoidAgent agent)
    {
        // handle data missmatch
        if (weights.Length != behaviors.Length)
        {
            Debug.LogError("Data missmatch in " + name, this);
            return Vector3.zero;
        }   

        //setup move
        Vector3 move = Vector3.zero;

        for (int i = 0; i < behaviors.Length; i++)
        {
            Vector3 partialMove = behaviors[i].CalculateMove(agent) * weights[i];

            if (partialMove != Vector3.zero)
            {
                if (partialMove.sqrMagnitude > weights[i] * weights[i])
                {
                    partialMove.Normalize();
                    partialMove *= weights[i];
                }

                move += partialMove;
            }
        }        

        return move;
    }
}