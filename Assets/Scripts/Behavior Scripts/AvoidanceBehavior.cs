using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Boid/Behavior/Avoidance")]
public class AvoidanceBehavior : FilterBoidBehavior
{
    public override Vector3 CalculateMove(BoidAgent agent)
    {
        List<Transform> neighbors = agent.AgentNeighborsTransform;
        // if no neighbor return no adjustement
        if (neighbors.Count == 0)
            return Vector3.zero;
        
        // add all point together and average
        Vector3 avoidanceMove = Vector3.zero;

        int numberOfAvoid = 0;
        List<Transform> filteredNearby = (filter == null) ? neighbors : filter.Filter(agent, neighbors);
        foreach (Transform agentTransform in filteredNearby)
        {
            if (Vector3.SqrMagnitude(agentTransform.position - agent.transform.position) < agent.AgentManager.SqrAvoidRadius)
            {
                numberOfAvoid++;
                avoidanceMove += (agent.transform.position - agentTransform.position);
            }
        }

        // Normalise movement by numberOfNearbyAgent
        if (numberOfAvoid > 0)
            avoidanceMove /= numberOfAvoid;
            
        return avoidanceMove;
    }
}