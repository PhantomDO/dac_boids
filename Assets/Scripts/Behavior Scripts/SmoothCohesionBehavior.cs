using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Boid/Behavior/Smooth Cohesion")]
public class SmoothCohesionBehavior : FilterBoidBehavior
{
    private Vector3 currentVelocity;
    public float agentSmoothTime = .5f;

    public override Vector3 CalculateMove(BoidAgent agent)
    {
        List<Transform> neighbors = agent.AgentNeighborsTransform;
        // if no neighbor return no adjustement
        if (neighbors.Count == 0)
            return Vector3.zero;
        
        // add all point together and average
        Vector3 cohesionMove = Vector3.zero;
        List<Transform> filteredNearby = (filter == null) ? neighbors : filter.Filter(agent, neighbors);
        foreach (Transform agentTransform in filteredNearby)
        {
            cohesionMove += agentTransform.position;
        }

        cohesionMove /= neighbors.Count;

        //create offset from agent.position
        cohesionMove -= agent.transform.position;

        // Smooth the movement 
        cohesionMove = Vector3.SmoothDamp(agent.transform.forward, cohesionMove, ref currentVelocity, agentSmoothTime);
        return cohesionMove;
    }
}