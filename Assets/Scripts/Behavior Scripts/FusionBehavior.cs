using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine.Jobs;
using Unity.Mathematics;

[CreateAssetMenu(menuName = "Boid/Behavior/Fusion")]
public class FusionBehavior : FilterBoidBehavior
{
    public Vector3[] behaviors = new Vector3[4];
    public float[] weights = new float[4];
    public Vector3 center;
    public float radius = 15f;

    private Vector3 currentVelocity;
    public float agentSmoothTime = .5f;

    public override Vector3 CalculateMove(BoidAgent agent)
    {  
        //setup move
        Vector3 move = Vector3.zero;
        
            behaviors[0] = Vector3.zero; // cohesion
            behaviors[1] = agent.transform.forward; // alignment
            behaviors[2] = Vector3.zero; // avoidance
            
            Vector3 centerOffset = center - agent.transform.position;
            float t = centerOffset.magnitude / radius;
            if (t < 0.9f) behaviors[3] = Vector3.zero;
            else behaviors[3] = centerOffset * t * t;
            
            if (agent.AgentNeighbors > 0)
            {
                behaviors[1] = Vector3.zero;

                int agentsAvoided = 0;
                List<Transform> filteredNearby = (filter == null) ? agent.AgentNeighborsTransform : filter.Filter(agent, agent.AgentNeighborsTransform);
                for (int i = 0; i < filteredNearby.Count; i++)
                {
                    behaviors[0] += filteredNearby[i].position;
                    behaviors[1] += filteredNearby[i].transform.forward;

                    if (Vector3.SqrMagnitude(filteredNearby[i].position - agent.transform.position) < agent.AgentManager.SqrAvoidRadius)
                    {
                        agentsAvoided++;
                        behaviors[2] += (agent.transform.position - filteredNearby[i].position);
                    }
                }    

                behaviors[0] /= agent.AgentNeighbors;
                behaviors[0] -= agent.transform.position;// Smooth the movement 
                behaviors[0] = Vector3.SmoothDamp(agent.transform.forward, behaviors[0], ref currentVelocity, agentSmoothTime);

                behaviors[1] /= agent.AgentNeighbors;

                if (agentsAvoided > 0) 
                    behaviors[2] /= agentsAvoided;
            }

            for (int i = 0; i < behaviors.Length; i++)
            {
                Vector3 partialMove = behaviors[i] * weights[i];

                if (partialMove != Vector3.zero)
                {
                    if (partialMove.sqrMagnitude > weights[i] * weights[i])
                    {
                        partialMove.Normalize();
                        partialMove *= weights[i];
                    }

                    move += (partialMove);
                }
            }
        
           
        return move;
    }
}