using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;

using Unity.Jobs;
using Unity.Collections;


[CreateAssetMenu(menuName = "Boid/Behavior/Alignment")]
public class AlignmentBehavior : FilterBoidBehavior
{
    public override Vector3 CalculateMove(BoidAgent agent)
    {
        List<Transform> neighbors = agent.AgentNeighborsTransform;
        // if no neighbor maintain current alignment
        if (neighbors.Count == 0)
            return agent.transform.forward;
        
        // add all point together and average
        Vector3 alignmentMove = Vector3.zero;
        List<Transform> filteredNearby = (filter == null) ? neighbors : filter.Filter(agent, neighbors);
        foreach (Transform agentTransform in filteredNearby)
        {
            alignmentMove += agentTransform.transform.forward;
        }

        alignmentMove /= neighbors.Count;
        return alignmentMove;
    }
}