using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Boid/Behavior/Cohesion")]
public class CohesionBehavior : FilterBoidBehavior
{
    public override Vector3 CalculateMove(BoidAgent agent)
    {
        List<Transform> neighbors = agent.AgentNeighborsTransform;
        // if no neighbor return no adjustement
        if (neighbors.Count == 0)
            return Vector3.zero;
        
        // add all point together and average
        Vector3 cohesionMove = Vector3.zero;
        List<Transform> filteredNearby = (filter == null) ? neighbors : filter.Filter(agent, neighbors);
        foreach (Transform agentTransform in filteredNearby)
        {
            cohesionMove += agentTransform.position;
        }

        // Normalise movement by numberOfNearbyAgent
        cohesionMove /= neighbors.Count;

        //create offset from agent.position
        cohesionMove -= agent.transform.position;
        return cohesionMove;
    }
}