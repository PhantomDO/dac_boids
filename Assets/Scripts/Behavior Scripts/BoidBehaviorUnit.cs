using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Jobs;

[BurstCompile]
public struct MoveJob : IJobParallelForTransform
{
    public NativeArray<Vector3> move;
    public float deltaTime;

    public float movingSpeed, sqrMaxSpeed, maxSpeed;

    public void Execute(int index, TransformAccess transform)
    {
        move[index] *= movingSpeed;
        
        if (move[index].sqrMagnitude > sqrMaxSpeed)
            move[index] = move[index].normalized * maxSpeed;

        transform.rotation = Quaternion.FromToRotation(Vector3.forward, move[index]);
        transform.position = move[index] * deltaTime;
    }
}