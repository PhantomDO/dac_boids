﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using UnityEngine.Jobs;
using Unity.Collections;

public class Boid : MonoBehaviour
{
    public BoidBehavior behavior;
    public bool useJobs = false;
    public BoidAgent agentPrefab;
    private List<BoidAgent> agents = new List<BoidAgent>();
    private Transform[] agentsTr;
    private NativeArray<Vector3> agentsV3;

    [Range(10, 100000)]
    public int boidsNumber = 50;
    private const float AGENTDENSITY = 0.08f;

    [Range(1f, 100f)]
    public float movingSpeed = 10f;
    [Range(1f, 100f)]
    public float maxSpeed = 5f;
    [Range(1f, 10f)]
    public float neighborRadius = 1.5f;
    [Range(0f, 1f)]
    public float avoidRadiusMultiplier = 0.5f;

    private float sqrMaxSpeed;
    private float sqrNeighborRadius;
    private float sqrAvoidRadius;
    public float SqrMaxSpeed { get { return sqrMaxSpeed; } }
    public float SqrAvoidRadius { get { return sqrAvoidRadius; } }

    public LayerMask searchLayer;

    #region JOBS
    NativeArray<Vector3> moves;
    TransformAccessArray transAccArr;

    JobHandle moveHandles;
    #endregion

    /*private void Start() 
    {
        InitializeNativeCollections();
        InitializeBoids();
    } 
    
    private void InitializeBoids()
    {
        sqrMaxSpeed = maxSpeed * maxSpeed;
        sqrNeighborRadius = neighborRadius * neighborRadius;
        sqrAvoidRadius = sqrNeighborRadius * (avoidRadiusMultiplier * avoidRadiusMultiplier);

        agentsTr = new Transform[boidsNumber];

        for (int i = 0; i < boidsNumber; i++)
        {
            BoidAgent newAgent = Instantiate(
                agentPrefab, 
                Random.insideUnitSphere * boidsNumber * AGENTDENSITY,
                Quaternion.Euler(Vector3.forward * Random.Range(0, 360)), 
                transform
            );
            newAgent.name = "Agent" + i;
            newAgent.Initialize(this);
            agents.Add(newAgent);
            agentsTr[i] = newAgent.transform;
            agentsV3[i] = newAgent.transform.position;
        }

        transAccArr = new TransformAccessArray(agentsTr);
    }

    private void InitializeNativeCollections()
    {
        agentsV3 = new NativeArray<Vector3>(boidsNumber, Allocator.Persistent);
        moves = new NativeArray<Vector3>(boidsNumber, Allocator.TempJob);
    }


    private void Update() 
    {
        if (Input.GetKeyDown(KeyCode.Space)) useJobs = !useJobs;
        MoveBoidsAgent();    
    }

    public void MoveBoidsAgent()
    {
        //float startTime = Time.realtimeSinceStartup;
        
        if (useJobs)
        {

            for (int i = 0; i < boidsNumber; i++)
            {
                BoidAgent agent = agents[i];
                //List<Transform> nearby = agent.GetNearbyObject(neighborRadius);
                //moves[i] = behavior.CalculateMove(agent, nearby, this);   
                moves[i] = agent.GetNearbyObject(neighborRadius, searchLayer);             
            }
            
            var moveJob = new MoveJob
            {
                move = moves,
                deltaTime = Time.deltaTime,

                sqrMaxSpeed = sqrMaxSpeed,
                maxSpeed = maxSpeed,
                movingSpeed = movingSpeed
            };

            moveHandles = moveJob.Schedule(transAccArr);
            JobHandle.ScheduleBatchedJobs();
        }
        else
        {
            for (int i = 0; i < boidsNumber; i++)
            {
                BoidAgent agent = agents[i];
                //List<Transform> nearby = agent.GetNearbyObject(neighborRadius, searchLayer);
                
                Vector3 move = agent.GetNearbyObject(neighborRadius, searchLayer);
                //move = behavior.CalculateMove(agent, nearby, this);
                    
                move *= movingSpeed;
                
                if (move.sqrMagnitude > sqrMaxSpeed)
                    move = move.normalized * maxSpeed;
                
                agent.Move(move);
                
            }
        }
        //Debug.Log(((Time.realtimeSinceStartup - startTime) * 1000f) + "ms"); 
    }

    public void LateUpdate()
    {
        moveHandles.Complete();
    }

    private void OnDestroy()
    {
        agentsV3.Dispose();
        moves.Dispose();
        transAccArr.Dispose();
    }*/
}
