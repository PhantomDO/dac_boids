﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public struct CollisionComponent : IComponentData
{
    public Entity source;
}
