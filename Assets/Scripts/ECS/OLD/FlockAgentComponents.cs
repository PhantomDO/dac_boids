using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity;
using Unity.Mathematics;
using Unity.Collections;

public struct FlockPositionComponent : IComponentData
{
    public float3 position;
}

public struct FlockMoveSpeedComponent : IComponentData
{
    public float moveSpeed;
}

public struct StayInRadiusComponent : IComponentData
{
    public float3 center;
    public float radius;
}

public struct AgentsComponent : IComponentData
{
    public Entity agent;
    //public NativeArray<Entity> agents;
}

public struct AgentSettingsComponent : IComponentData
{
    public float cohesionForce;
    public float alignmentForce;
    public float avoidanceForce;
    public float stayInRadiusForce;
}