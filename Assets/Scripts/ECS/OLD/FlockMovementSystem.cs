using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class FlockMovementSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref Translation translation, ref FlockMoveSpeedComponent moveSpeedComponent,
        ref StayInRadiusComponent stayInRadius, ref FlockPositionComponent flockPosition) => {

            float3 backToCenter = BackToCenter(stayInRadius, flockPosition);

            translation.Value.x += backToCenter.x * moveSpeedComponent.moveSpeed * Time.deltaTime;
            translation.Value.y += backToCenter.y * moveSpeedComponent.moveSpeed * Time.deltaTime;
            translation.Value.z += backToCenter.z * moveSpeedComponent.moveSpeed * Time.deltaTime;
            
            flockPosition.position = new float3(translation.Value.x, translation.Value.y, translation.Value.z);
        });
    }

    /*private float3 Cohesion(AgentsComponent agentsComponent, FlockPositionComponent flockPosition)
    {
        
    }*/

    private float3 BackToCenter(StayInRadiusComponent stayInRadius, FlockPositionComponent flockPosition)
    {
        Vector3 centerOffset = stayInRadius.center - flockPosition.position;
        float t = centerOffset.magnitude / stayInRadius.radius;
        if (t < 0.9f)
        {
            return Vector3.zero;
        }

        return centerOffset * t * t;
    }
}