using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Rendering;
using Unity.Transforms;

public class FlockManager : MonoBehaviour
{
    private NativeArray<Entity> entityArray;

    [SerializeField] private Mesh mesh;
    [SerializeField] private Material material;

    [SerializeField] private int entityNumber;
    [SerializeField] private float moveSpeed;
    private const float AGENTDENSITY = .8f;

    [SerializeField] private Vector3 center;
    [SerializeField] private float radius;
    
    [SerializeField] private float cohesionForce;
    [SerializeField] private float alignmentForce;
    [SerializeField] private float avoidanceForce;
    [SerializeField] private float stayInRadiusForce;

    private void Start() 
    {
        EntityManager manager = World.Active.EntityManager;

        EntityArchetype entityArchetype = manager.CreateArchetype(
            typeof(AgentsComponent),
            typeof(AgentSettingsComponent),
            typeof(FlockPositionComponent),
            typeof(FlockMoveSpeedComponent),
            typeof(StayInRadiusComponent),
            typeof(Translation),
            typeof(RenderMesh),
            typeof(LocalToWorld)
        );

        entityArray = new NativeArray<Entity>(entityNumber, Allocator.Temp);
        manager.CreateEntity(entityArchetype, entityArray);

        for (int i = 0; i < entityArray.Length; i++)
        {
            Entity entity = entityArray[i];
            float3 rdm = UnityEngine.Random.insideUnitSphere * ((entityNumber) * AGENTDENSITY);

            manager.SetComponentData(entity, new AgentsComponent {
                agent = entity,
                //agents = entityArray
            });

            manager.SetComponentData(entity, new AgentSettingsComponent {
                cohesionForce = cohesionForce,
                alignmentForce = alignmentForce,
                avoidanceForce = avoidanceForce,
                stayInRadiusForce = stayInRadiusForce
            });

            manager.SetComponentData(entity, new FlockPositionComponent {
                position = rdm
            });

            manager.SetComponentData(entity, new FlockMoveSpeedComponent {
                moveSpeed = moveSpeed
            });

            manager.SetComponentData(entity, new StayInRadiusComponent {
                center = center,
                radius = radius
            });

            manager.SetComponentData(entity, new Translation {
                Value = rdm
            });

            manager.SetSharedComponentData(entity, new RenderMesh {
                mesh = mesh,
                material = material
            });
        }

        entityArray.Dispose();
        
    }

    void OnDisable()
    {
      /* EntityManager manager = World.Active.EntityManager;

        entityArray = manager.GetAllEntities();
        
        foreach(var e in entityArray)
        {
            AgentsComponent agents = manager.GetComponentData<AgentsComponent>(e);
            agents.agents.Dispose();
        }*/
    }

}