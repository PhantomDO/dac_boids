﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Entities;
using Unity.Transforms;
using Unity.Collections;

public class Boid_ECS : MonoBehaviour
{
    public bool useEcs = false;
    public BoidBehavior behavior;
    public bool useUnit = false;
    public BoidAgent_ECS agentPrefab;
    private List<BoidAgent_ECS> agents = new List<BoidAgent_ECS>();

    [Range(10, 10000)]
    public int boidsNumber = 50;
    private const float AGENTDENSITY = 0.008f;

    [Range(1f, 100f)]
    public float movingSpeed = 10f;
    [Range(1f, 100f)]
    public float maxSpeed = 5f;
    [Range(1f, 10f)]
    public float neighborRadius = 1.5f;
    [Range(0f, 1f)]
    public float avoidRadiusMultiplier = 0.5f;

    [HideInInspector]
    public float sqrMaxSpeed;
    [HideInInspector]
    public float sqrNeighborRadius;
    [HideInInspector]
    public float sqrAvoidRadius;
    [HideInInspector]
    public NativeArray<Entity> entityArray;
    public float SqrAvoidRadius { get { return sqrAvoidRadius; } }


    EntityManager entityManager;
    Entity boidEntityAgent;

    private void Start() 
    {
        if (useEcs)
        {
            entityManager = World.Active.EntityManager;
            boidEntityAgent = GameObjectConversionUtility.ConvertGameObjectHierarchy(agentPrefab.gameObject, World.Active);
            SpawnBoidEcs();
        }
        else
        {
            InitializeBoids();
        }
    }

    private void Update() 
    {
        if (useEcs)
        {

        }
        else
        {
            MoveBoidsAgent();
        }
              
    }
    void SpawnBoidEcs()
    {
        sqrMaxSpeed = maxSpeed * maxSpeed;
        sqrNeighborRadius = neighborRadius * neighborRadius;
        sqrAvoidRadius = sqrNeighborRadius * (avoidRadiusMultiplier * avoidRadiusMultiplier);

        entityArray = new NativeArray<Entity>(boidsNumber, Allocator.TempJob);
        entityManager.Instantiate(boidEntityAgent, entityArray);

        for (int i = 0; i < boidsNumber; i++)
        {
            entityManager.AddComponent(entityArray[i], typeof(CollisionComponent));

            entityManager.AddComponent(entityArray[i], typeof(BoidData));

            entityManager.AddComponent(entityArray[i], typeof(Translation));

            entityManager.AddComponent(entityArray[i], typeof(Rotation));

            entityManager.SetComponentData(entityArray[i], new BoidData
            {
                HasBeenJobbed = false
            });

            entityManager.SetComponentData(entityArray[i], new CollisionComponent
            {
                source = entityArray[i]
            });

            entityManager.SetComponentData(entityArray[i],
                new Translation
                {
                    Value = UnityEngine.Random.insideUnitSphere * boidsNumber * AGENTDENSITY
                });

            entityManager.SetComponentData(entityArray[i],
                new Rotation
                {
                    Value = Quaternion.Euler(Vector3.forward * Random.Range(0, 360))
                });
            
        }
        entityArray.Dispose();
    }

    private void InitializeBoids()
    {
        sqrMaxSpeed = maxSpeed * maxSpeed;
        sqrNeighborRadius = neighborRadius * neighborRadius;
        sqrAvoidRadius = sqrNeighborRadius * (avoidRadiusMultiplier * avoidRadiusMultiplier);

        for (int i = 0; i < boidsNumber; i++)
        {
            BoidAgent_ECS newAgent = Instantiate(
                agentPrefab, 
                Random.insideUnitSphere * boidsNumber * AGENTDENSITY,
                Quaternion.Euler(Vector3.forward * Random.Range(0, 360)), 
                transform
            );
            newAgent.name = "Agent" + i;
            newAgent.Initialize(this);
            agents.Add(newAgent);
        }
    }

    public void MoveBoidsAgent()
    {
        //Supprimé
    }
}
