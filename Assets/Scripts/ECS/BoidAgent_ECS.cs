using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[RequireComponent(typeof(Collider))]
public class BoidAgent_ECS : MonoBehaviour, IConvertGameObjectToEntity
{
    Boid_ECS agentBoid;
    public Boid_ECS AgentBoid { get { return agentBoid; } }

    Collider agentCollider;
    public Collider AgentCollider { get { return agentCollider; } }

    
    void Start()
    {
        agentCollider = GetComponent<Collider>();    
    }

    public void Initialize(Boid_ECS boid)
    {
        agentBoid = boid;
    }

    public void Move(Vector3 velocity)
    {
        transform.forward = velocity;
        transform.position += (Vector3)velocity * Time.deltaTime;
    }
    public void MoveECS(Vector3 velocity)
    {
        transform.forward = velocity;
        transform.position += (Vector3)velocity * Time.deltaTime;
    }

    public List<Transform> GetNearbyObject(float neighborRadius)
    {
        // Return nearby transform of agents near the agent tested
        List<Transform> nearby = new List<Transform>();
        Collider[] nearbyColliders = Physics.OverlapSphere(
            this.transform.position, 
            neighborRadius
        );

        foreach (Collider c in nearbyColliders)
            if (c != this.AgentCollider)
                nearby.Add(c.transform);

        return nearby;
    }
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        
        dstManager.AddComponent(entity, typeof(BoidData));

    }
}