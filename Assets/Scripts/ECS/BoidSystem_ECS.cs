﻿using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;

public class BoidSystem_ECS : JobComponentSystem
{
    [ReadOnly]
    NativeArray<float> m_weights;
    [ReadOnly]
    NativeArray<Vector3> m_behaviors;
    [ReadOnly]
    NativeArray<Entity> m_entityArray;
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        m_weights = new NativeArray<float>(4, Allocator.TempJob);
        m_behaviors = new NativeArray<Vector3>(4,Allocator.TempJob);
        m_entityArray = World.Active.EntityManager.GetAllEntities(Allocator.TempJob);                        //GetAllEntities(Allocator.TempJob);
        
        m_weights[0] = 0.5f;
        m_weights[1] = 1.5f;
        m_weights[2] = 15f;
        m_weights[3] = 0.1f;

        CalculateMove move = new CalculateMove { radius = 15f, 
            entityArray = m_entityArray,
            weights = m_weights,
            behaviors = m_behaviors
        };
        JobHandle job = move.Schedule(this,inputDeps);
        job.Complete();

        //m_weights.Dispose();
        //m_entityArray.Dispose();
        //m_behaviors.Dispose();
        

        return job;
    }

    [BurstCompile] // Inutile car Burst ne peut pas interpreter les Translations et le World.Active
    struct CalculateMove : IJobForEach<Translation, CollisionComponent>
    {
        [NativeDisableParallelForRestriction] [DeallocateOnJobCompletionAttribute] public NativeArray<float> weights;  //DeallocateOnJobCompletionAttribute = Dispose automatiquement nos NativeArray a la completion du job
        [NativeDisableParallelForRestriction] [DeallocateOnJobCompletionAttribute] [ReadOnly] public NativeArray<Entity> entityArray;
        [NativeDisableParallelForRestriction] [DeallocateOnJobCompletionAttribute] public NativeArray<Vector3> behaviors;
        public Vector3 center;
        private Vector3 currentVelocity;
        public float radius;

        public void Execute(ref Translation pos, [ReadOnly] ref CollisionComponent col)
        {
            BoidData hasBeenExecuted = World.Active.EntityManager.GetComponentData<BoidData>(col.source); // on recupere le component data de l'instance depuis le monde actif
            if (hasBeenExecuted.HasBeenJobbed == false)
            {
                World.Active.EntityManager.SetComponentData(col.source, new BoidData { HasBeenJobbed = true }); //Modification du component data de l'instance 
                radius = 15f;
                Vector3 move = Vector3.zero;

                behaviors[0] = Vector3.zero; // cohesion
                float NormPos = (pos.Value.x * pos.Value.x) + (pos.Value.y * pos.Value.y) + (pos.Value.z * pos.Value.z);

                behaviors[1] = pos.Value / NormPos;// alignement
                behaviors[2] = Vector3.zero; // avoidance

                Vector3 centerOffset = center - (Vector3)pos.Value;
                float t = centerOffset.magnitude / radius;

                if (t < 0.9f) 
                    behaviors[3] = Vector3.zero;
                else 
                    behaviors[3] = centerOffset * t * t;

                //if (agent.AgentNeighbors > 0)
                if (GetNearbyObject(1, col.source, entityArray).Count > 0)
                {
                    behaviors[1] = Vector3.zero;

                    int agentsAvoided = 0;
                    //List<Transform> filteredNearby = (filter == null) ? agent.AgentNeighborsTransform : filter.Filter(agent, agent.AgentNeighborsTransform);
                    List<Translation> filteredNearby = GetNearbyObject(1, col.source, entityArray);
                    for (int i = 0; i < filteredNearby.Count; i++)
                    {
                        behaviors[0] += (Vector3)filteredNearby[i].Value;
                        float Norm = (filteredNearby[i].Value.x * filteredNearby[i].Value.x) + (filteredNearby[i].Value.y * filteredNearby[i].Value.y) + (filteredNearby[i].Value.z * filteredNearby[i].Value.z);
                        behaviors[1] += (Vector3)(filteredNearby[i].Value / Norm);

                        if (Vector3.SqrMagnitude(filteredNearby[i].Value - pos.Value) < .5f)
                        {
                            agentsAvoided++;
                            behaviors[2] += (Vector3)(pos.Value - filteredNearby[i].Value);
                        }

                    }

                    behaviors[0] /= GetNearbyObject(1, col.source, entityArray).Count;
                    behaviors[0] -= (Vector3)pos.Value;// Smooth the movement 
                    NormPos = (pos.Value.x * pos.Value.x) + (pos.Value.y * pos.Value.y) + (pos.Value.z * pos.Value.z);
                    behaviors[0] = Vector3.SmoothDamp((pos.Value / NormPos), behaviors[0], ref currentVelocity, .5f);

                    behaviors[1] /= GetNearbyObject(1, col.source, entityArray).Count;

                    if (agentsAvoided > 0)
                        behaviors[2] /= agentsAvoided;
                }

                for (int i = 0; i < 4; i++)
                {
                    Vector3 partialMove = behaviors[i] * weights[i];

                    if (partialMove != Vector3.zero)
                    {
                        if (partialMove.sqrMagnitude > weights[i] * weights[i])
                        {
                            partialMove.Normalize();
                            partialMove *= weights[i];
                        }

                        move += (partialMove);
                    }
                }
                pos.Value += (float3)move * 10 * Time.deltaTime;
                
            }
            //weights.Dispose();
            //entityArray.Dispose();
            //behaviors.Dispose();
        }

        public  List<Translation> GetNearbyObject([ReadOnly] float neighborRadius, [ReadOnly] Entity thisEnt, [ReadOnly] NativeArray<Entity> entities)
        {
            EntityManager entityManager = World.Active.EntityManager; //On récupere l'entity Manager depuis le monde actif
            List<Translation> nearby = new List<Translation>();
            foreach (var entity in entities)
            {
                if (entity != thisEnt)
                {
                    Translation thisTrans = entityManager.GetComponentData<Translation>(entity);
                    Translation entTrans = entityManager.GetComponentData<Translation>(thisEnt);

                    float distanceBetween = math.sqrt(Mathf.Pow((entTrans.Value.x - thisTrans.Value.x), 2) + Mathf.Pow((entTrans.Value.y - thisTrans.Value.y), 2) + Mathf.Pow((entTrans.Value.z - thisTrans.Value.z), 2));
                    //float distanceBetween = math.sqrt(Mathf.Pow((entityManager.GetComponentData<Translation>(thisEnt).Value.x - entityManager.GetComponentData<Translation>(entity).Value.x), 2) + Mathf.Pow((entityManager.GetComponentData<Translation>(thisEnt).Value.y - entityManager.GetComponentData<Translation>(entity).Value.y), 2) + Mathf.Pow((entityManager.GetComponentData<Translation>(thisEnt).Value.z - entityManager.GetComponentData<Translation>(entity).Value.z), 2));
                    if (distanceBetween < neighborRadius)
                    {
                        nearby.Add(entTrans);
                        //nearby.Add(entityManager.GetComponentData<Translation>(thisEnt));
                    }
                }
            }
           // entities.Dispose();
            return nearby;
        }
    }
}
