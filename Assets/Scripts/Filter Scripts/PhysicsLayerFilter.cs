using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Boid/Filter/Physics Layer")]
public class PhysicsLayerFilter : ContextFilter
{
    public LayerMask mask;

    public override List<Transform> Filter(BoidAgent agent, List<Transform> original)
    {
        List<Transform> filtered = new List<Transform>();

        foreach (Transform item in original)
        {
            if  (mask != (mask | (1 << item.gameObject.layer)))
            {
                if (!filtered.Contains(item))
                    filtered.Add(item);
            }
        }

        return filtered;
    }
}