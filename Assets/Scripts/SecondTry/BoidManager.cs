using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using UnityEngine.Jobs;
using Unity.Collections;

public class BoidManager : MonoBehaviour
{
    private const float AGENTDENSITY = 0.08f;

    public bool useJobs;
    public BoidBehavior behavior;
    public ComputeShader computeShader;
    private int kernelHandle;

    public BoidAgent prefab;
    private BoidAgent[] agents;
    public BoidAgent[] Agents { get { return agents;} }
    private NativeArray<Vector3> Velocity;
    private TransformAccessArray transformAccessArray;
    private GPUBoid[] GPUAgents;
    

    // SETTINGS
    [Range(10, 100000)]
    public int boidsNumber = 50;
    [Range(1f, 100f)]
    public float movingSpeed = 10f;
    [Range(1f, 100f)]
    public float maxSpeed = 5f;
    [Range(1f, 10f)]
    public float neighborRadius = 1.5f;
    [Range(0f, 1f)]
    public float avoidRadiusMultiplier = 0.5f;
    [Range(0f, 20f)]
    public float cohesionForce = 0.5f;
    [Range(0f, 20f)]
    public float alignmentForce = 1.5f;
    [Range(0f, 20f)]
    public float avoidanceForce = 15f;
    [Range(0f, 20f)]
    public float centerForce = 0.1f;

    public float centerRadius = 15f;

    public LayerMask searchLayer;

    // SQR SETTINGS
    private float sqrMaxSpeed;
    private float sqrNeighborRadius;
    private float sqrAvoidRadius;
    public float SqrNeighborRadius { get { return sqrNeighborRadius; } }
    public float SqrMaxSpeed { get { return sqrMaxSpeed; } }
    public float SqrAvoidRadius { get { return sqrAvoidRadius; } }



    private void Start() 
    {
        sqrMaxSpeed = maxSpeed * maxSpeed;
        sqrNeighborRadius = neighborRadius * neighborRadius;
        sqrAvoidRadius = sqrNeighborRadius * (avoidRadiusMultiplier * avoidRadiusMultiplier);

        agents = new BoidAgent[boidsNumber];
        GPUAgents = new GPUBoid[boidsNumber];
        transformAccessArray = new TransformAccessArray(boidsNumber);
        
        Velocity = new NativeArray<Vector3>(boidsNumber , Allocator.TempJob);
        
        kernelHandle = computeShader.FindKernel("CSMain");

        SpawnBoids();
    }

    private void Update() 
    {
        MoveBoids();

        if (Input.GetKeyDown(KeyCode.Space))
            useJobs = !useJobs;
    }
    
    private void OnDestroy()
    {
        transformAccessArray.Dispose();
        Velocity.Dispose(); 
    }
    
    private void SpawnBoids()
    {
        for (int i = 0; i < boidsNumber; i++)
        {
            BoidAgent newAgent = Instantiate(
                prefab, 
                transform.position + (Random.insideUnitSphere * 50 * AGENTDENSITY),
                Quaternion.Euler(Vector3.forward * Random.Range(0, 360)), 
                transform
            );
            newAgent.name = "Agent (" + i + ")";
            newAgent.AgentManager = this;
            agents[i] = (newAgent);

            Velocity[i] = agents[i].AgentVelocity;
            transformAccessArray.Add(agents[i].transform);

            GPUAgents[i].pos = agents[i].transform.position;
            GPUAgents[i].rot = agents[i].transform.forward;
        }
    }

    private void MoveBoids()
    {
        if (!useJobs)
        {
            for (int i = 0; i < boidsNumber; i++)
            {
                BoidAgent agent = agents[i];

                agent.AgentNeighborsTransform = BoidsOverlapSphere(agent, neighborRadius, searchLayer);

                Vector3 movement = behavior.CalculateMove(agent);

                agent.AgentNextMovement(movement);
            }
        } 
        else
        {
            // CALCULATE COHESION/ALIGMENT/AVOIDANCE FOR EACH BOIDS
            GPUCalculateMove();
            // MOVE THE POSITION AND ROTATION OF THE TRANSFORM 
            AgentNextMoveJob moveJob = new AgentNextMoveJob
            {
                Movement = Velocity,
                SqrMaxSpeed = SqrMaxSpeed,
                MaxSpeed = maxSpeed,
                MovingSpeed = movingSpeed,
                DeltaTime = Time.deltaTime
            };

            var handle = moveJob.Schedule(transformAccessArray);
            handle.Complete();
        }       
    }

    public void GPUCalculateMove()
    {
        //float startTime = Time.realtimeSinceStartup;
        for (int i = 0; i < boidsNumber; i++)
        {
            GPUAgents[i].pos = agents[i].transform.position;
            GPUAgents[i].rot = agents[i].transform.forward;
        }

        ComputeBuffer buffer = new ComputeBuffer(boidsNumber, 76);

        buffer.SetData(GPUAgents);


        computeShader.SetInt("boidsCount", boidsNumber);
        
        computeShader.SetFloat("nearbyDis", SqrNeighborRadius);
        computeShader.SetFloat("avoidDis", SqrAvoidRadius);
        computeShader.SetFloat("radius", centerRadius);
        computeShader.SetFloat("deltaTime", Time.deltaTime);

        computeShader.SetBuffer(kernelHandle, "boidBuffer", buffer);
        computeShader.Dispatch(kernelHandle, boidsNumber / 256 + 1, 1, 1);
        buffer.GetData(GPUAgents);  
        buffer.Release(); 

        for (int i = 0; i < boidsNumber; i++)
        {
            agents[i].AgentCohesion = GPUAgents[i].cohesion;
            agents[i].AgentAlignment = GPUAgents[i].alignment;
            agents[i].AgentAvoidance = GPUAgents[i].avoidance;
            agents[i].AgentCenter = GPUAgents[i].center;
            agents[i].AgentNeighbors = GPUAgents[i].neighbors;

            agents[i].UpdateBoidSettings();

            Velocity[i] = agents[i].AgentVelocity;
        }
        //Debug.Log(((Time.realtimeSinceStartup - startTime) * 1000f) + "ms");     

    }

    public List<Transform> BoidsOverlapSphere(BoidAgent agent, float radius, LayerMask layer)
    {
        agent.AgentNeighbors = 0;

        List<Transform> nearbyColliders = new List<Transform>();

        Collider[] near = Physics.OverlapSphere(
            agent.transform.position, 
            radius,
            layer
        );
        
        for (int i = 0; i < near.Length; i++)
        {   
            if (agent.AgentCollider != near[i])
            {
                agent.AgentNeighbors++;
                nearbyColliders.Add(near[i].transform);
            }   
        }

        return nearbyColliders;
    }

    
}